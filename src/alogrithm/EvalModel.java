package alogrithm;

import chess.Board;
import chess.Piece;

import java.util.Map;

public class EvalModel {
    private int[] values = new int[2];

    public int eval(Board board, char player) {
        for (Map.Entry<String, Piece> stringPieceEntry : board.pieces.entrySet()) {
            Piece piece = stringPieceEntry.getValue();
            int[] reversePosition = new int[]{board.BOARD_HEIGHT - 1 - piece.position[0], piece.position[1]};
            switch (piece.character) {
                case 'b':
                    if (piece.color == 'r'){
                        values[0] += 8888;
                    }
                    else {
                        values[1] += 8888;
                    }
                    break;
                case 's':
                    if (piece.color == 'r'){
                        values[0] += evalPiecePosition(1, piece.position);
                    }
                    else {
                        values[1] += evalPiecePosition(1, reversePosition);
                    }
                    break;
                case 'x':
                    if (piece.color == 'r'){
                        values[0] += evalPiecePosition(2, piece.position);
                    }
                    else{
                        values[1] += evalPiecePosition(2, reversePosition);
                    }
                    break;
                case 'm':
                    if (piece.color == 'r') {
                        values[0] += evalPiecePosition(3, piece.position);
                    } else {
                        values[1] += evalPiecePosition(3, reversePosition);
                    }
                    break;
                case 'j':
                    if (piece.color == 'r') {
                        values[0] += evalPiecePosition(4, piece.position)*2;
                    } else {
                        values[1] += evalPiecePosition(4, reversePosition)*2;
                    }
                    break;
                case 'p':
                    if (piece.color == 'r') {
                        values[0] += evalPiecePosition(5, piece.position);
                    } else {
                        values[1] += evalPiecePosition(5, reversePosition);
                    }
                    break;
                case 'z':
                    if (piece.color == 'r') {
                        values[0] += evalPiecePosition(6, piece.position);
                    } else {
                        values[1] += evalPiecePosition(6, reversePosition);
                    }
                    break;
            }
        }
        switch (player) {
            case 'r':
                return values[0] - values[1];
            case 'b':
                return values[1] - values[0];
            default:
                return -1;
        }
    }

    private int evalPiecePosition(int p, int[] pos) {
        int[][] sPosition = new int[][]{
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},

                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0,20, 0,20, 0, 0, 0},
                {0, 0, 0, 0,23, 0, 0, 0, 0},
                {0, 0, 0,20, 0,20, 0, 0, 0}
        };
        int[][] xPosition = new int[][]{
                {0,  0,  0,  0,  0,  0,  0,  0,  0},
                {0,  0,  0,  0,  0,  0,  0,  0,  0},
                {0,  0,  0,  0,  0,  0,  0,  0,  0},
                {0,  0,  0,  0,  0,  0,  0,  0,  0},
                {0,  0,  0,  0,  0,  0,  0,  0,  0},
                {0,  0, 20,  0,  0,  0, 20,  0,  0},
                {0,  0,  0,  0,  0,  0,  0,  0,  0},
                {18,  0,  0,  0, 23,  0,  0,  0, 18},
                {0,  0,  0,  0,  0,  0,  0,  0,  0},
                {0,  0, 20,  0,  0,  0, 20,  0,  0},
        };
        int[][] pPosition = new int[][]{
                {206,208,207,213,214,213,207,208,206},
                {206,212,209,216,233,216,209,212,206},
                {206,208,207,214,216,214,207,208,206},
                {206,213,213,216,216,216,213,213,206},
                {208,211,211,214,215,214,211,211,208},
                {208, 212, 212, 214, 215, 214, 212, 212, 208},
                {204, 209, 204, 212, 214, 212, 204, 209, 204},
                {198, 208, 204, 212, 212, 212, 204, 208, 198},
                {200, 208, 206, 212, 200, 212, 206, 208, 200},
                {194, 206, 204, 212, 200, 212, 204, 206, 194}
        };
        int[][] mPosition = new int[][]{
                {90, 90, 90, 96, 90, 96, 90, 90, 90},
                {90, 96,103, 97, 94, 97,103, 96, 90},
                {92, 98, 99,103, 99,103, 99, 98, 92},
                {93,108,100,107,100,107,100,108, 93},
                {90,100, 99,103,104,103, 99,100, 90},

                {90, 98,101,102,103,102,101, 98, 90},
                {92, 94, 98, 95, 98, 95, 98, 94, 92},
                {93, 92, 94, 95, 92, 95, 94, 92, 93},
                {85, 90, 92, 93, 78, 93, 92, 90, 85},
                {88, 85, 90, 88, 90, 88, 90, 85, 88}
        };
        int[][] jPosition = new int[][]{
                {100, 100,  96, 91,  90, 91,  96, 100, 100},
                {98,  98,  96, 92,  89, 92,  96,  98,  98},
                {97,  97,  96, 91,  92, 91,  96,  97,  97},
                {96,  99,  99, 98, 100, 98,  99,  99,  96},
                {96,  96,  96, 96, 100, 96,  96,  96,  96},

                {95,  96,  99, 96, 100, 96,  99,  96,  95},
                {96,  96,  96, 96,  96, 96,  96,  96,  96},
                {97,  96, 100, 99, 101, 99, 100,  96,  97},
                {96,  97,  98, 98,  98, 98,  98,  97,  96},
                {96,  96,  97, 99,  99, 99,  97,  96,  96}
        };
        int[][] zPosition = new int[][]{
                {9,  9,  9, 11, 13, 11,  9,  9,  9},
                {19, 24, 34, 42, 44, 42, 34, 24, 19},
                {19, 24, 32, 37, 37, 37, 32, 24, 19},
                {19, 23, 27, 29, 30, 29, 27, 23, 19},
                {14, 18, 20, 27, 29, 27, 20, 18, 14},

                {7,  0, 13,  0, 16,  0, 13,  0,  7},
                {7,  0,  7,  0, 15,  0,  7,  0,  7},
                {0,  0,  0,  0,  0,  0,  0,  0,  0},
                {0,  0,  0,  0,  0,  0,  0,  0,  0},
                {0,  0,  0,  0,  0,  0,  0,  0,  0}
        };
        if ( p == 0 ) return 8888;
        if ( p == 1 ) return sPosition[pos[0]][pos[1]];
        if ( p == 2 ) return xPosition[pos[0]][pos[1]];
        if (p == 3) return mPosition[pos[0]][pos[1]];
        if (p == 4) return jPosition[pos[0]][pos[1]];
        if (p == 5) return pPosition[pos[0]][pos[1]];
        if (p == 6) return zPosition[pos[0]][pos[1]];
        return -1;
    }
}
