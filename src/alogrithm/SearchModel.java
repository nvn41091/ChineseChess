package alogrithm;

import chess.Board;
import chess.Piece;
import chess.Rules;
import control.GameController;

import java.util.ArrayList;
import java.util.Map;

public class SearchModel {
    private static int DEPTH = 2;
    private Board board;
    private GameController controller = new GameController();

    public AlphaBetaNode search(Board board) {
        this.board = board;
        if (board.pieces.size() < 28)
            DEPTH = 3;
        if (board.pieces.size() < 16)
            DEPTH = 4;
        if (board.pieces.size() < 6)
            DEPTH = 5;
        if (board.pieces.size() < 4)
            DEPTH = 6;
        AlphaBetaNode best = null;
        ArrayList<AlphaBetaNode> moves = generateMovesForAll(true);
        for (AlphaBetaNode n : moves) {
            /* Di chuyển */
            Piece eaten = board.updatePiece(n.piece, n.to);
            n.value = alphaBeta(DEPTH, Integer.MIN_VALUE, Integer.MAX_VALUE, false);
            /* Chọn nước di chuyển tốt nhất */
            if (best == null || n.value >= best.value)
                best = n;
            /* Quay lại nước di chuyển */
            board.updatePiece(n.piece, n.from);
            if (eaten != null) {
                board.pieces.put(eaten.key, eaten);
                board.backPiece(eaten.key);
            }
        }
        return best;
    }


    private int alphaBeta(int depth, int alpha, int beta, boolean isMax) {
        if (depth == 0 || controller.hasWin(board) != 'x')
            return new EvalModel().eval(board, 'b');
        ArrayList<AlphaBetaNode> moves = generateMovesForAll(isMax);

        synchronized (this) {
            for (final AlphaBetaNode n : moves) {
                Piece eaten = board.updatePiece(n.piece, n.to);
                final int finalBeta = beta;
                final int finalAlpha = alpha;
                final int finalDepth = depth;
                final int[] temp = new int[1];
                if (depth == 2) {
                    if (isMax) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                int alphaBeta = alphaBeta(finalDepth - 1, finalAlpha, finalBeta, false);
                                temp[0] = Math.max(finalAlpha, alphaBeta);
                            }
                        }).run();
                        alpha = temp[0];
                    } else {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                int alphaBeta = alphaBeta(finalDepth - 1, finalAlpha, finalBeta, true);
                                temp[0] = Math.min(finalBeta, alphaBeta);
                            }
                        }).run();
                        beta = temp[0];
                    }
                }
                else {
                    if (isMax) alpha = Math.max(alpha, alphaBeta(depth - 1, alpha, beta, false));
                    else beta = Math.min(beta, alphaBeta(depth - 1, alpha, beta, true));
                }
                board.updatePiece(n.piece, n.from);
                if (eaten != null) {
                    board.pieces.put(eaten.key, eaten);
                    board.backPiece(eaten.key);
                }
                if (beta <= alpha) break;
            }
        }
        return isMax ? alpha : beta;
    }

    private ArrayList<AlphaBetaNode> generateMovesForAll(boolean isMax) {
        ArrayList<AlphaBetaNode> moves = new ArrayList<AlphaBetaNode>();
        for (Map.Entry<String, Piece> stringPieceEntry : board.pieces.entrySet()) {
            Piece piece = stringPieceEntry.getValue();
            if (isMax && piece.color == 'r') continue;
            if (!isMax && piece.color == 'b') continue;
            for (int[] nxt : Rules.getNextMove(piece.key, piece.position, board))
                moves.add(new AlphaBetaNode(piece.key, piece.position, nxt));
        }
        return moves;
    }

}
